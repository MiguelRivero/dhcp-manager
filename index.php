<?php

include('conexion.php');

?>

<html lang="es">
<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<meta name="robot" content="NOINDEX, NOFOLLOW, NOSNIPPET, NOODP,NOYODP">
	<title>DHCP Config</title>
</head>
<body>


	<div class="container">
		<!-- Contenido cabecera -->
		<?php include 'cabecera.php'; ?>
		<?php
			if (isset($_SESSION['mensaje']) && $_SESSION['mensaje'] != null && $_SESSION['mensaje'] != "")
			{
				echo '<p class="text-warning text-right" >'.$_SESSION['mensaje'].'</p>';
				$_SESSION['mensaje'] = null;
			}
		?>
		<div class="content">
			<div  id="navlateral">
			<!--Barra de Navegacion-->
				<ul class="nav nav-tabs nav-pills" >
					<li><a href="#subred33">Subred 33</a></li>
					<li><a href="#subred34">Subred 34</a></li>
					<li><a href="#subred35">Subred 35</a></li>
				</ul>
				<div class="span5">
				<?php  // Para contar el numero de ips libres
					$resultado= $bd->query('select count(*) from dhcp where mac is NULL and id_dhcp < 769;');
					$fila = $resultado->fetchArray();
					$ip_libre=$fila['count(*)'];
					echo "<p> There are <span class=\"destacado\">".$ip_libre."</span> free IP address</p>";
				?>
				</div>
				<div class="text-right">
					<a href="estadisticas.php" class="btn btn-primary" style="float:left;">Statistics</a>
					<form id="exportarDHCP" action="guardarDhcp.php" method="get" class="form-inline" onClick="javascript:isEmptyFile()">
						<input name="nombreArchivo" id="nombreArchivo" type="text" class="input span4" placeholder="file name">
						<button class="btn btn-primary" type="submit" > <i class="icon-upload icon-white"></i> Export dhcp</button>
					</form>
				</div>
			</div>
			<!-- Fin Navegación-->

			<div   data-spy="scroll" data-target="#navlateral"><!--Body content-->
				<br/>
				<div id="scrolled">
				<table class="table"  >
					<tr>
						<th id="icon">#</th>
						<th id="ip">IP</th>
						<th id="ip">MAC</th>
						<th id="ip">Type</th>
						<th id="ip">Library</th>
						<th id="ip">Hostname</th>
						<th id="icon">Edit</th>
						<th id="icon">Remove</th>
					</tr>
					<?php
					$number = 0;
					$resultado = $bd->query('SELECT id_dhcp,ip,mac,acro_mod,nombre_bib,hostname FROM dhcp as d join bibliotecas as b join modelos as m where d.id_modelo=m.id_modelo and d.id_biblioteca=b.id_biblioteca;');
						while ($fila = $resultado->fetchArray())
						{
							$number++;
							if ($fila['id_dhcp']==1) {
								echo "<tr id=\"subred33\"><td colspan=\"8\"><b>Subred 33</b></td></tr>";
							}elseif ($fila['id_dhcp']==257) {
								echo "<tr id=\"subred34\"><td colspan=\"8\"><b>Subred 34</b></td></tr>";
							}elseif ($fila['id_dhcp']==513) {
								echo "<tr id=\"subred35\"><td colspan=\"8\"><b>Subred 35</b></td></tr>";
							}

							if( (isset($fila['mac']) && $fila['mac']==NULL && isset($fila['host']) && $fila['host'] ==NULL) || (isset($fila['mac']) && $fila['mac']=="" && isset($fila['host']) && $fila['host'] =="") ){
								echo "<tr class=\"success\" >";
							}
							else
							{
								echo "<tr class=\"info\" >";
							}
							?>
								<td class="text-right" > <?php echo $fila['id_dhcp'] ?>      </td>
								<td class="text-right" > <?php echo $fila['ip'] ?>      </td>
								<td class="text-center"> <?php echo $fila['mac'] ?>     </td>
								<td class="text-center"> <?php echo $fila['acro_mod'] ?></td>
								<td class="text-center"> <?php echo $fila['nombre_bib'] ?>    </td>
								<td class="text-center"> <?php echo $fila['hostname']?></td>
								<?php
								if( $number==1020 || $number==1021 || $number==1022 || $number==1023 || $number==1024){
									echo "<td></td><td></td>";
								}else{
								?>
									<td class="text-center">
										<a href="editar_ip.php?id=<?php echo $fila['id_dhcp'] ?>"  class="btn" ><i class="icon-pencil"></i></a>
									</td>
								<td align="center">
									<a href="liberar_ip.php?id=<?php echo $fila['id_dhcp']; ?>" class="btn"><i class="icon-trash"></i></a>
								</td>
								<?php
								}
								?>
							</tr>
						<?php
						} // cierra el while
						?>
				</table>
				</div>
			</div>


		</div><!-- Cierra content-->
	</div> <!-- Cierra container-->

	<div  class="gotop">
			<a href="#" class="fixedSidebar"><i class="icon-chevron-up icon-white"></i> Go Top</a>
	</div>


</body>
</html>
<?php

	// Close db conection
	if (!$bd->close()) {
		echo "<script>alert('The database was not closed properly');</script>";
	}
?>