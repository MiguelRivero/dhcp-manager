<?php

include('conexion.php');

?>

<html lang="es">
<head>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<meta name="robot" content="NOINDEX, NOFOLLOW, NOSNIPPET, NOODP,NOYODP">
	<title>DHCP Config</title>
</head>
<body>

	<div class="container">
		<!-- Contenido cabecera -->
		<?php include 'cabecera.php'; ?>
		<div class="content">
			<?php
				// Para contar el numero de ips libres
				$resultado= $bd->query('SELECT count(*) FROM dhcp WHERE mac is NULL AND id_dhcp < 769;');
				$fila = $resultado->fetchArray();
				$ip_libre=$fila['count(*)'];
				echo "<p> There are <span class=\"destacado\">".$ip_libre."</span> free ip addresses</p>";
				echo "<a href=\"index.php\" class=\"btn btn-primary\" style=\"float:right;\">Volver</a><hr>";

				?><div id="izq">
				<?php
					$resultado= $bd->query('SELECT id_modelo,nombre_mod FROM modelos ORDER BY id_modelo');
					while ($fila = $resultado->fetchArray()) {
						$sentencia="SELECT count(*) FROM dhcp where id_modelo='".$fila['id_modelo']."'";
						$resultado2=$bd->query($sentencia);
						$fila2 = $resultado2->fetchArray();
						$num=$fila2['count(*)'];

						echo "<p> There are <span class=\"destacado\">".$num."</span> PC type ".$fila['nombre_mod']."</p><br>";
					}
				?>
				</div>
				<div id="dch">
				<?php
					$resultado= $bd->query('SELECT id_biblioteca,nombre_bib FROM bibliotecas ORDER BY id_biblioteca');
					while ($fila = $resultado->fetchArray()) {
						$sentencia2="SELECT count(*) FROM dhcp where id_biblioteca='".$fila['id_biblioteca']."'";
						$resultado3=$bd->query($sentencia2);
						$fila3 = $resultado3->fetchArray();
						$num2=$fila3['count(*)'];

						echo "<p> There are <span class=\"destacado\">".$num2."</span> PCs in ".$fila['nombre_bib']." </p><br>";
					}
					echo "<hr>"
				?>
				</div>
		</div>
	</div>
</body>