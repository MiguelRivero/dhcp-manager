<?php

include('conexion.php');


$id=$_GET['id'];
$resultado = $bd->query("SELECT id_dhcp,ip,mac,id_modelo,id_biblioteca,hostname,nombre,comentario,es_modelo FROM dhcp  WHERE id_dhcp='$id';");
while ($fila = $resultado->fetchArray())
	{
		$_SESSION['id'] = $fila['id_dhcp'];
		$db_ip = $fila['ip'];
		$db_ip_hidden=$fila['ip'];
		$db_mac = $fila['mac'];
		$db_id_modelo= $fila['id_modelo'];
		$db_id_biblioteca = $fila['id_biblioteca'];
		$db_hostname = $fila['hostname'];
		$db_nombre = $fila['nombre'];
		$db_comentario = $fila['comentario'];
		$db_es_modelo = $fila['es_modelo'];
	}



$error_mac=$error_id_modelo=$error_id_biblioteca=$error_nombre=$error_comentario="";
// aquí entra cuando se envia el formulario y se hacen todas las comprobaciones
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	//$id=$_SESSION['id'];s

	$db_ip            = $_REQUEST['input_ip_hidden'];
	$db_input_mac     = $_REQUEST["input_mac"];
	$db_ip_hidden     = $_REQUEST['input_ip_hidden'];
	$db_mac           = $_REQUEST["input_mac"];
	$db_id_modelo     = $_REQUEST['input_id_modelo'];
	$db_id_biblioteca = $_REQUEST['input_id_biblioteca'];
	$db_nombre        = $_REQUEST['input_nombre'];
	$db_comentario    = $_REQUEST['input_comentario'];
	$db_es_modelo     = $_REQUEST['input_es_modelo'];



	// Se comprueba que la mac sea correcta  ##############################################################
	if (empty($db_mac))
	{
		$error_mac = "Este campo es obligatorio";
	}
	else if ( preg_match("/^[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}$/i", $db_input_mac)!=1) {
    	$error_mac = "El formato de la mac debe ser:  00:00:00:00:00:00 ";
	}else{
		$input_mac= strtoupper (htmlspecialchars (trim ( $_REQUEST["input_mac"] )));
	}



	if ($db_id_modelo == 0){
		$error_id_modelo ="Debe elegir un modelo de equipo.";
	}else{
		$input_id_modelo     = htmlspecialchars (trim ( $_REQUEST['input_id_modelo'] ));
	}


	if ($db_id_biblioteca == 0)	{
		$error_id_biblioteca ="Debe elegir la biblioteca del equipo.";
	}else{
		$input_id_biblioteca = htmlspecialchars (trim ( $_REQUEST['input_id_biblioteca'] ));
	}

	if ( strlen( $db_nombre ) > 15)
	{
		$error_nombre="Este campo no puede tener más de 15 carácteres.";
	}else{
		$input_nombre        = strtoupper (htmlspecialchars (trim ( $_REQUEST['input_nombre'] )));
	}

	if ( strlen( $db_comentario ) > 50) {
		$error_comentario="Este campo no puede tener más de 50 carácteres.";
	}else{
		$input_comentario    = htmlspecialchars (trim ( $_REQUEST['input_comentario'] ));
	}

	$input_ip_hidden     = htmlspecialchars (trim ( $_REQUEST['input_ip_hidden'] ));
	$input_es_modelo     = htmlspecialchars (trim ( $_REQUEST['input_es_modelo'] ));
	if($input_es_modelo==""){
		$input_es_modelo=0;
	}

	// echo "<pre>";
	// print_r(get_defined_vars());
	// echo "</pre>";
	// si no hay errores en los valores se ejecutará la modificación ##############################################################
	if ( ($error_mac == "") && ($error_id_modelo == "") && ($error_id_biblioteca == "") && ($error_nombre == "") && ($error_comentario == "")  )
	{
		//Componemos la variable db_hostname
		$consulta= $bd->query('SELECT acro_mod FROM modelos WHERE id_modelo='.$db_id_modelo);
		$fila = $consulta->fetchArray();
		$input_hostname=$fila['acro_mod'];

			//por la estructura decidida del hostname nos quedamos con los 2 ultimas cifras de la ip
			$numerosip=explode(".", $db_ip);
		$input_hostname.="-".$numerosip[2]."_".$numerosip[3];
	    $sql = "UPDATE dhcp SET mac='".$input_mac."', id_modelo='".$input_id_modelo."', id_biblioteca='".$input_id_biblioteca."', hostname='".$input_hostname."', dated=(DATETIME('NOW','localtime')),  nombre='".$input_nombre."', comentario='".$input_comentario."', es_modelo='".$input_es_modelo."' WHERE id_dhcp='".$_SESSION['id']."';";

		echo $sql;

		if ( $bd->exec($sql)){
			$_SESSION['mensaje'] = 'La dirección '.$db_ip.' se ha modificado correctamente';
			$bd->close();
			header("Location: index.php");
		}
		else{
			$_SESSION['mensaje'] = 'La dirección '.$db_ip.' NO se ha modificado correctamente';
		}
	}
}
?>

<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
	<meta name="robot" content="NOINDEX, NOFOLLOW, NOSNIPPET, NOODP,NOYODP">
	<title>IP edition</title>
	<script type="text/javascript" src="js/mijs.js"></script>


</head>
<body>
	<div class="container">
		<!-- Contenido cabecera -->
		<?php include 'cabecera.php'; ?>
		<div class="content">
			<form name="formulario_editarip"  class="form-horizontal" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
				<fieldset>
				<legend>IP edition</legend>
					<div class="control-group">
						<label class="control-label" for="input_ip">IP</label>
						<div class="controls">
							<input type="text" name="input_ip" id="input_ip" value="<?php echo htmlspecialchars($db_ip); ?>" disabled >
							<!-- El siguiente campo se ha creado por que el estado disabled del input anterior NO permite que se pueda recuperar su info -->
							<input type="hidden" name="input_ip_hidden" id="input_ip_hidden" value="<?php echo htmlspecialchars($db_ip_hidden); ?>" >
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="input_mac">MAC</label>
						<div class="controls">
							<input type="text" name="input_mac" id="input_mac" value="<?php echo htmlspecialchars($db_mac); ?>" onblur="javascript:compruebaMac();">
							<span id="errormac" class="text-error text-right"><?php echo $error_mac; ?></span>
						</div>
					</div>
					<!-- <div class="control-group">
						<label class="control-label" for="input_host">Host</label>
						<div class="controls">
							<input type="text" name="input_host" id="input_host" value="<?php echo htmlspecialchars($db_host); ?>"  onblur="javascript:compruebahost();">
							<span id="errorhost" class="text-error text-right"><?php echo $error_host; ?></span>
						</div>
					</div> -->
					<div class="control-group">
						<label class="control-label" for="input_id_modelo">Type</label>
						<div class="controls">
							<select name="input_id_modelo" id="input_id_modelo" onchange="javascript:compruebaModelo();">
								<?php
									$resultado = $bd->query("SELECT * FROM modelos;");
									while ($fila = $resultado->fetchArray())
									{
										$selected="";
										if($db_id_modelo==$fila['id_modelo']) {
											$selected= "selected ";
										}
										echo "<option ".$selected." value=".$fila['id_modelo'].">".$fila['nombre_mod']."</option>";
									}
								?>
							</select>
							<span id="erroridmodelo" class="text-error text-right"><?php echo $error_id_modelo; ?></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="input_id_biblioteca">Library</label>
						<div class="controls">
							<select name="input_id_biblioteca" id="input_id_biblioteca" onchange="javascript:compruebaBiblioteca();">
								<?php
									$resultado = $bd->query("SELECT * FROM bibliotecas;");
									while ($fila = $resultado->fetchArray())
									{
										$selected="";
										if($db_id_biblioteca==$fila['id_biblioteca']) {
											$selected= "selected ";
										}
										echo "<option ".$selected." value=".$fila['id_biblioteca'].">".$fila['nombre_bib']."</option>";
									}
								?>
							</select>
							<span id="erroridbiblioteca" class="text-error text-right"><?php echo $error_id_biblioteca; ?></span>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label" for="input_nombre">Name</label>
						<div class="controls">
							<input type="text" name="input_nombre" id="input_nombre" value="<?php echo htmlspecialchars($db_nombre); ?>" onblur="javascript:compruebaNombre();">
							<span id="errornombre" class="text-error text-right"><?php echo $error_nombre; ?></span>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="input_comentario">Coment</label>
						<div class="controls">
							<textarea rows="3" name="input_comentario" id="input_comentario" value="<?php echo htmlspecialchars($db_comentario); ?>" onblur="javascript:compruebaComentario();"></textarea>
							<span id="errorcomentario" class="text-error text-right"><?php echo $error_comentario; ?></span>
						</div>
					</div>
					<div class="contro-group">
						<label class="control-label" for="input_es_modelo">Is model</label>
						<div class="controls">
							<input type="checkbox" name="input_es_modelo" value="1" <?php if( $db_es_modelo!=0 ){echo "checked";}?> >
						</div>
					</div>

					<!-- <div class="control-group">
						<label class="control-label" for="input_hostname">Host Name</label>
						<div class="controls">
							<input type="text" name="input_hostname" id="input_hostname" value="<?php echo htmlspecialchars($db_hostname); ?>"  onblur="javascript:compruebahostname();">
							<span id="errorhostname" class="text-error text-right"><?php echo $error_hostname; ?></span>
						</div>
					</div> -->
				</fieldset>
				<div class="text-center">
					<input type="button" class="btn" name="volver_Atras" value="Back" onclick="volver();">
					<input type="button" class="btn" name="liberar_ip" value="Remove" onclick="location.href='liberar_ip.php?id=<?php echo $_SESSION['id']; ?>'"
					<?php if(isset($db_mac) && $db_mac==NULL && isset($db_old_mac) && $db_old_mac==NULL && isset($db_hostname) && $db_hostname==NULL){
						echo "disabled";
					}
					?>
					>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</body>
</html>

<!-- <script language='JavaScript'>
    alert("PDFs generados correctamente");
    window.location = 'index.php';
</script> -->

<?php
	// CERRAR LA CONEXION DE LA BBDD
	if (!$bd->close()) {
		echo "<script>alert('La base de datos no se ha cerrado correctamente');</script>";
	}
?>