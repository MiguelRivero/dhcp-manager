function compruebaMac()
{
	var input_mac=document.getElementById("input_mac").value;
	var patron=/^[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}$/i;
	var error=document.getElementById("errormac");
	error.className="text-success";
	error.innerHTML="<i class='icon-ok'></i>  MAC Válida";
	if (patron.test(input_mac)==false) {
		error.className="text-error text-righ";
		error.innerHTML="<i class='icon-remove'></i> La mac introducida es incorrecta";
	};
	if (input_mac==="") {
		error.className="text-warning";
		error.innerHTML="<i class='icon-warning-sign'></i> Este campo es obligatorio";
	}
}

function compruebaModelo()
{
	var input_modelo=document.getElementById("input_id_modelo").value;
	var error=document.getElementById("erroridmodelo");
	if (input_modelo==0) {
		error.className="text-warning";
		error.innerHTML="<i class='icon-warning-sign'></i> Es obligatorio elegir un modelo";
	}else if (input_modelo>0) {
		error.className="text-success";
		error.innerHTML="<i class='icon-ok'></i>  Modelo Válido";
	};
}

function compruebaBiblioteca()
{
	var input_biblioteca=document.getElementById("input_id_biblioteca").value;
	var error=document.getElementById("erroridbiblioteca");
	if (input_biblioteca==0) {
		error.className="text-warning";
		error.innerHTML="<i class='icon-warning-sign'></i> Es obligatorio elegir una biblioteca";
	}else if (input_biblioteca > 0) {
		error.className="text-success";
		error.innerHTML="<i class='icon-ok'></i> Biblioteca Válida";
	}
}

function compruebaNombre()
{
	var input_nombre=document.getElementById("input_nombre").value;
	var error=document.getElementById("errornombre");
	// error.className="text-success";
	// error.innerHTML="<i class='icon-ok'></i>  Nombre Válido";
	var longitud=15; // variable para definir el numero máximo de caracteres del hostname
	if (input_nombre.length>longitud) {
		error.className="text-error text-righ";
		error.innerHTML="<i class='icon-remove'></i> El nombre debe tener un máximo de "+longitud+" caracteres";
	};
}

function compruebaComentario()
{
	var input_comentario=document.getElementById("input_comentario").value;
	var error=document.getElementById("errorcomentario");
	// error.className="text-success";
	// error.innerHTML="<i class='icon-ok'></i>  Comentario Válido";
	var longitud=50; // variable para definir el numero máximo de caracteres del hostname
	if (input_comentario.length>longitud) {
		error.className="text-error text-righ";
		error.innerHTML="<i class='icon-remove'></i> El comentario debe tener un máximo de "+longitud+" caracteres";
	};
}

function volver(){
	window.location = 'index.php';
}

function isEmptyFile(){
	var input=document.getElementById("nombreArchivo").value;
	if (input.length<1) {
		var form=document.getElementById("exportarDHCP");
		var div=document.createElement("div");
		form.appendChild(div);
		div.className="text-error";
		div.innerHTML="Debe elegir un archivo.";
	};
}